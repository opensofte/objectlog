package org.sweetie.objectlog.test.service.impl;
/*
 * FileName: ObjectOperationServiceImpl
 * Author gouhao
 */

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.sweetie.objectlog.core.annotation.LogPoint;
import org.sweetie.objectlog.core.enums.OperationEnum;
import org.sweetie.objectlog.test.mapper.SysRoleMapper;
import org.sweetie.objectlog.test.model.SysRoleModel;
import org.sweetie.objectlog.test.service.SysRoleService;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRoleModel> implements SysRoleService {

    @Override
    @LogPoint(serviceHandler = SysRoleServiceImpl.class, entityHandler = SysRoleModel.class,
            operation = OperationEnum.COMPLEX, moduleName = "sysRole", remark = "addAssociate")
    public void logUpdateById(SysRoleModel roleModel) {
        this.updateById(roleModel);
    }

    @Override
    @LogPoint(serviceHandler = SysRoleServiceImpl.class, entityHandler = SysRoleModel.class,
            operation = OperationEnum.COMPLEX, moduleName = "sysRole", remark = "updateAssociate")
    public void logInsert(SysRoleModel roleModel) {
        this.insert(roleModel);
    }
}
