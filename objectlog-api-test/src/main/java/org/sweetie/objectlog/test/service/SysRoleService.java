package org.sweetie.objectlog.test.service;
/*
 * FileName: ObjectOperationService
 * Author gouhao
 */

import com.baomidou.mybatisplus.service.IService;
import org.sweetie.objectlog.test.model.SysRoleModel;

public interface SysRoleService extends IService<SysRoleModel> {
    void logUpdateById(SysRoleModel roleModel);

    void logInsert(SysRoleModel roleModel);
}
