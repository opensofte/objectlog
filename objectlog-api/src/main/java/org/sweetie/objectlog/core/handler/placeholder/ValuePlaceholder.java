package org.sweetie.objectlog.core.handler.placeholder;

import org.sweetie.objectlog.core.handler.AbstractAttributeValueHandler;

/**
 * 占位符
 *
 * @Author: gouhao
 * @Date: 2025/03/10/11:58
 * @Description:
 */
public class ValuePlaceholder extends AbstractAttributeValueHandler {

}
