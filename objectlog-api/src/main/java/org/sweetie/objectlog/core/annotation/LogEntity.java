package org.sweetie.objectlog.core.annotation;

/*
 * FileName: LogEntity
 * Author gouhao
 */

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.sweetie.objectlog.core.enums.AttributeTypeEnum;
import org.sweetie.objectlog.core.handler.AttributeTypeHandler;
import org.sweetie.objectlog.core.handler.AttributeValueHandler;
import org.sweetie.objectlog.core.handler.placeholder.EnumPlaceholder;
import org.sweetie.objectlog.core.handler.placeholder.HandlerPlaceholder;
import org.sweetie.objectlog.core.handler.placeholder.TypePlaceholder;
import org.sweetie.objectlog.core.handler.placeholder.ValuePlaceholder;

import java.lang.annotation.*;

@Inherited
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogEntity {
    String alias() default "";

    AttributeTypeEnum type() default AttributeTypeEnum.NORMAL;

    String extendedType() default "";

    Class<? extends AttributeTypeHandler> typeHandler() default TypePlaceholder.class;

    Class<? extends AttributeValueHandler> valueHandler() default ValuePlaceholder.class;

    Class<?> enumType() default EnumPlaceholder.class;

    Class<? extends ServiceImpl> targetHandler() default HandlerPlaceholder.class;

    String targetHandlerPath() default "";

    String extractField() default "";


}
