package org.sweetie.objectlog.core.handler.placeholder;

import org.sweetie.objectlog.core.ObjectFieldWrapper;
import org.sweetie.objectlog.core.handler.AbstractAttributeTypeHandler;
import org.sweetie.objectlog.core.model.ObjectAttributeModel;

/**
 * 占位符
 *
 * @Author: gouhao
 * @Date: 2025/03/10/11:57
 * @Description:
 */
public class TypePlaceholder extends AbstractAttributeTypeHandler {
    @Override
    public ObjectAttributeModel handlerAttributeChange(ObjectFieldWrapper fieldWrapper) {
        return null;
    }
}
